# Burn In Tester
I was once part of a discussion in [Syd Heresy's twitch channel](https://www.twitch.tv/sydheresy/)
on how analog electronics like synthesizers often need to be run for up to 24 hours to
"burn in" the components.  The device may behave erratically until enough time has passed.
I looked around online to see if anyone had done any study into how exactly burn in works but found very little at the time.
I decided I could get some hard numbers for myself with an arduino.  This repo is my code and documentation for how I chose to test which components are more subject to "burn in".

## The Setup (Hardware)
My inital idea was that I'd have an arduino put current through some components and monitor the change in the current over time.
In practice I have a PWM signal flowing through the component under test and a 220 ohm resistor.  The resistor goes to ground. I'm unsure if it is better to connect the component to an analog pin on the arduino or to ground, so I'm doing initial tests of both configurations and will choose whichever seems
most consistent.
I also have the same signal going to A0 and a 220 ohm resistor to ground in order to help control for confounding variables such as variation from
the microcontroller, jumpers, and breadboard.
Knowing that temperature
can cause deviation in analog electronics, I added a temperature sensor.  My temp sensor also measures humidity, so I'm logging humidity just cuz.
I have a real time clock to add some consistency to the log. I'm powering the setup (including the clock) via USB.

![Wiring Diagram](https://gitlab.com/TommyTorty10/burn-in-tester/-/raw/default/wiring_diagram.png "Wiring Diagram")

### Components
* [SD Card Shield](https://core-electronics.com.au/w5100-ethernet-sd-card-shield-arduino-compatible.html)
* [Arduino Microcontroller](https://store-usa.arduino.cc/products/arduino-leonardo-with-headers?selectedStore=us)
* [DS1307 Real Time Clock](https://arduinogetstarted.com/tutorials/arduino-ds1307-rtc-module)
* [DHT11 Temperature and Humidity Sensor 3 pin variant](https://www.circuitbasics.com/how-to-set-up-the-dht11-humidity-sensor-on-an-arduino/)
* 2 220 ohm resistors
* breadboard
* breadboard jumpers
* microSD card
* USB Cable

## Firmware
I used the old school Arduino IDE, and installed my libraries through its built-in library manager.
To do so yourself, go to Tools -> Manage Libraries.  Then you can type the names of libraries into the search bar and install them at the press of a button.
The versions of the libraries you use is unlikely to matter, and you should try the most recent for security and stability.
The libraries are:
* TinyDHT (version 1.1.0 by Adafruit)
* RTClib (version 2.1.1 by Adafruit  
The following libraries are likely already installed in your Arduino IDE, but I'm listing them here for security.
* SD (version 1.2.4 by Arduino)
* SPI

## Results
I let the device run while I wrote the code, and there was very little change in the voltage measured over time.  
Mostly TBA still.
