#include <SD.h>
#include <SPI.h>
#include "RTClib.h"
#include "TinyDHT.h"

int wirePin = A0;
int componentPin = A1;
int pwmPin = 3;
int wire, component, temp, hum;

// Temp/Humidity Sensor
DHT dht(8, DHT11);

// clock
RTC_DS1307 rtc;
DateTime now;
int lastTime;


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  delay(500);

  Serial.print("Initializing SD card...");

  // see if the card is present and can be initialized:
  if (!SD.begin(4)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  Serial.println("card initialized.");

  // RTC
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1) delay(10);
  }

  if (! rtc.isrunning()) {
    Serial.print("Setting correct time...");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    Serial.println("time set");
  }
  now = rtc.now();
  lastTime = now.minute();

  analogWrite(pwmPin, 255);
}


void loop() {
  // read time
  now = rtc.now();
//  Serial.println(lastTime);
  if (now.minute() == (lastTime+1)) {
    // read temp
    temp = temperature();
    // read humidity
    hum = humidity();
    // read wire
    wire = analogRead(wirePin);
    // read component
    component = analogRead(componentPin);
    // log to SD card
    writeToSD();
  
    Serial.print("wire: ");
    Serial.print(wire);
    Serial.print("\tcomponent: ");
    Serial.print(component);
//    Serial.print("\t\ttemp: ");
//    Serial.print(temp);
//    Serial.print("\thum: ");
//    Serial.print(hum);
//    Serial.print("\t\ttime: \t");
//    Serial.print(now.hour(), DEC);
//    Serial.print(":");
//    Serial.print(now.minute(), DEC);
//    Serial.print(":");
//    Serial.print(now.second(), DEC);
    Serial.print("\n");

    lastTime++;
  }else {
    delay(10000);
  }
}


void writeToSD() {
  File dataFile = SD.open("datalog.txt", FILE_WRITE);
  if (dataFile) {
    dataFile.print("Wire:\t");
    dataFile.print(wire);
    dataFile.print("\t\tComponent:\t");
    dataFile.print(component);
    dataFile.print("\t\tTemperature:\t");
    dataFile.print(temp);
    dataFile.print("\t\tHumidity:\t");
    dataFile.print(hum);
    dataFile.print("\t\tTime:\t");
    dataFile.print(now.hour(), DEC);
    dataFile.print(":");
    dataFile.print(now.minute(), DEC);
    dataFile.print(":");
    dataFile.print(now.second(), DEC);
    dataFile.println();
  } else {
    Serial.println(F("Failed to write to SD card"));
  }

  dataFile.close();
}


int humidity() {
  int8_t h = dht.readHumidity();
  if (h == BAD_HUM) {
    // TODO send to error state?
    Serial.println("Humidity Error");
  }
  return h;
}

int temperature() {
  // returns temperature in Fahrenheit
  int16_t t = dht.readTemperature(1);
  if (t == BAD_TEMP) {
    // TODO send to error state?
    Serial.println("Temperature error");
  }
  return t;
}
